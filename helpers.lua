--[[
ta-zettels-ng/helpers.lua

Copyright (c) 2022 Stefan Thesing

This file is part of ta-zettels-ng.

ta-zettels-ng is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ta-zettels-ng is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ta-zettels-ng. If not, see http://www.gnu.org/licenses/.
--]]
 
local M = {}; M.__index = M

local function construct()
  local self = setmetatable({}, M)
  return self
end
setmetatable(M, {__call = construct})

-- Pretty table printer for debugging
-- Print contents of `tbl`, with indentation.
-- `indent` sets the initial level of indentation.
function tprint(tbl, indent)
  if not indent then indent = 0 end
  for k, v in pairs(tbl) do
    formatting = string.rep("  ", indent) .. k .. ": "
    if type(v) == "table" then
      print(formatting)
      tprint(v, indent+1)
    else
      print(formatting .. tostring(v))
    end
  end
end

--- Receives a list of strings and converts it to one comma-separated string
-- @param tags List of strings
local function keyword_string(keywords)
    local s = nil
    for k, v in pairs(keywords) do
        if not s then
            s = tostring(v)
        else
            s = s .. ", " .. tostring(v)
        end
    end
    -- if keywords was empty, s is still nil. But in that case, we want to return
    -- an empty string.
    return s or ""
end

-- Catch the output of a call to an external tool (like zettels)
-- returns true if statuscode is 0 + the a table containing the output line by
-- line
local function catch_os_output(call)
    local temp = assert(io.popen(call, 'r'))
    outputstr = assert(temp:read('*a'))
    statuscode = temp:close()
    
    -- remove surrounding whitespace
    outputstr = string.gsub(outputstr, '^%s*(.-)%s*$', '%1')
    
    -- convert the lines into a table
    local output = {}
    for line in outputstr:gmatch("([^\n]*)\n?") do
        table.insert(output, line)
    end
    return statuscode, output
end


-- For the filtered dialog boxes of textadept, the respective fields of a
-- table must be lined up (col1, col2, col3, col1, col2, col3 and so on.)
-- This function does that with the "files"-part of zettels' index
-- or a subset of it.
function lineup(scope)
    -- In the dialog, we want to show the files ordered by filename. Thus 
    -- we have to sort the keys of files first:
    local keys = {}
    for k,v in pairs(scope) do
        table.insert(keys, k)
    end
    table.sort(keys)
    
    -- Now, for what this function is actually supposed to be doing, 
    -- we iterate over keys and use them to read the values of scope.
    local lined_up = {}
    for _, k in pairs(keys) do
        -- We want to fill an array with 'Title', 'Keywords', and 'File'
        lined_up[#lined_up+1] = scope[k].title
        lined_up[#lined_up+1] = keyword_string(scope[k].keywords)
        lined_up[#lined_up+1] = k
    end
    return lined_up
end


-- calls textadept's filtered dialog box and presents the index or a 
-- subset of it: scope
function search_items(searchby, scope, columns, items, rootdir)
    local searchcolumn = 1 -- Default for Title
    if (searchby == "Keywords") then
        searchcolumn   = 2
    elseif (searchby == "File") then
        searchcolumn   = 3
    end
    
    local button_or_exit, selection = 
      ui.dialogs.filteredlist{
        title = "Search by " .. searchby,
        informative_text = "Scope: " .. scope,
        text = "",
        columns = columns,
        search_column = searchcolumn,
        items = items,
        select_multiple=true, 
        string_output=true,
        output_column=3, --we want the filename(s)
      }
      
    if button_or_exit ~= "delete" then
        for _, zettel in pairs(selection) do
            --io.open_file(rootdir .. "/" .. zettel)
            io.open_file(rootdir .. zettel)
        end
    end
end


-- Turn an absolute path to a path relative to the zettelkasten's root 
-- directory. Will not yield satisfactory results if it points outside of the
-- root directory.
local function relpath(abspath, rootdir)
    -- First, we make sure that abspath is really absolute,
    -- then make it relative
    return string.gsub(lfs.abspath(abspath), rootdir, "")
end

-- ###########################################################################
-- Public interface
-- ###########################################################################
M = {
    tprint = tprint,
    lineup = lineup,
    search_items = search_items,
    catch_os_output = catch_os_output,
    relpath = relpath,
}

return M
