---
title:  'File 5'
keywords: []
followups: []
---

# Let's see

What happens if the metadata [block](../file1.md) doesn't end with three dots, 
but three dashes? And does it also work if the rest of the document contains 
"..."?
And "---", too?
And as a single line?
...
And "---", too?
---
