--[[
ta-zettels-ng/init.lua

Copyright (c) 2022 Stefan Thesing

This file is part of ta-zettels-ng.

ta-zettels-ng is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ta-zettels-ng is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ta-zettels-ng. If not, see http://www.gnu.org/licenses/.
--]]

-- ###########################################################################
-- Imports
-- ###########################################################################
--local path = require("path")

-- #################
-- #     lyaml     #
-- #################

local lyaml = lyaml
-- If it's not available globally, the local lyaml will now be nil
-- In that case, we look for other options

if not lyaml then
    -- maybe Textadept's yaml-Module is present, which includes lyaml
    if pcall(require, "yaml.lyaml") then
        lyaml = require("yaml.lyaml")
    else
    -- otherwise, let's hope the user has made a working lyaml.lua available
    -- to Textadept
        lyaml = require("lyaml")
    end
    -- if that isn't the case, either, Textadept will complain at startup.
end

-- #####################
-- # Helpers Submodule #
-- #####################
local helpers = require("ta-zettels-ng.helpers")

-- ############################################################################
-- Declare module-wide variables
-- ############################################################################

local rootdir        = nil  -- set by enable()
local indexfile      = nil  -- set by enable()
local config_file    = nil  -- set by enable()
local index          = nil  -- set refresh_index(), initally called by enable()
local lined_up_index = nil  -- set refresh_index(), initally called by enable()
local columns        = {'Title', 'Keywords', 'File'}

-- ############################################################################

--- ##########################################################################
--- Search & inspect functions
--- ##########################################################################

local function search_index(searchby)
    helpers.search_items(searchby, "Index", columns, lined_up_index, rootdir)
end

local function show_output(call, scope_string)
    local success, output = helpers.catch_os_output(call)
    if success then
        local outputlist = {}
        for _,filename in pairs(output) do
            outputlist[filename] = index["files"][filename]
        end
        
        helpers.search_items("Title", 
                            scope_string,
                            columns, 
                            helpers.lineup(outputlist),
                            rootdir)
    end
end

local function show_antecedents(filename)
    filename = helpers.relpath(filename, rootdir)
    local call = "zettels -a " .. filename
    show_output(call, "Antecedents of " .. filename)
end

local function show_followups(filename)
    filename = helpers.relpath(filename, rootdir)
    local call = "zettels -f " .. filename
    show_output(call, "Followups of " .. filename)
end

local function show_sequences(filename)
    filename = helpers.relpath(filename, rootdir)
    local call = "zettels -s " .. filename
    show_output(call, "Sequences of " .. filename)
end

local function show_sequencetree(filename)
    filename = helpers.relpath(filename, rootdir)
    local call = "zettels -S " .. filename
    show_output(call, "Sequencetree of " .. filename)
end

local function copy_rel_filename(filename)
    filename = helpers.relpath(filename, rootdir)
    buffer.copy_text(buffer, filename)
end

local function refresh_index(config_path)
    local call = "zettels -m -u -c " .. config_path
    print("Updating/refreshing index by calling: " .. call)
    --call zettels to silently update the index
    --local status_code = os.execute("zettels -m -u -c " .. config_file)
    local status_code = os.execute(call)
    if not status_code then
        print("Something went wrong when executing the system command 'zettels'")
        print(status_code)
    end
    
    --read the updated index file
    file = io.open(indexfile, "r")
    index = lyaml.load(file:read "*a")
    io.close(file)
    
    lined_up_index = helpers.lineup(index["files"])
    
end

--- ##########################################################################
--- The menus
--- ##########################################################################

-- Define Zettels Menu
local zettels_menu = {
  title = 'Zettels',
  {'Search by Title',       function() search_index('Title') end},
  {'Search by Keywords',    function() search_index('Keywords') end},
  {'Search by Filename',    function() search_index('File') end},
  {'Refresh index',         function() refresh_index(config_file) end},
}

-- Define Zettels Context Menu
local zettels_context_menu = {
    title = 'Zettel',
    {'Show followups', function() show_followups(buffer.filename) end},
    {'Show antecedents', function() show_antecedents(buffer.filename) end},
    {'Show sequences', function() show_sequences(buffer.filename) end},
    {'Show sequencetree', function() show_sequencetree(buffer.filename) end},
    {'Copy link to self to clipboard', function() copy_rel_filename(buffer.filename) end},
}

--- ##########################################################################
--- Enable the module
--- ##########################################################################
local function enable(config_path, separator)
    separator = separator or "/"
    -- let's write the config path the user specified to the module wide
    -- variable config_file. We'll use that from now on.
    if config_path then
        config_file = config_path
    else
        config_file = _USERHOME .. '/modules/ta-zettels-ng/examples/config/libzettels.cfg.yaml'
    end
        
    local file = io.open (config_file, "r")
    local config = lyaml.load(file:read "*a")
    io.close(file)
    rootdir   = config["rootdir"]
    --TODO different separators
    --if not (string.sub(rootdir,-1) == "/") then
    if not (string.sub(rootdir,-1) == separator) then
        rootdir = rootdir .. separator
    end
    indexfile = config["indexfile"]

    -- For more complex search operations, we just
    -- call zettels. But for basic operations, as
    -- opening by file name and searching by title or 
    -- keywords. For that, we line up the current index
    -- in a three colum table for textadepts filtered_dialog
    -- boxes.
    
    refresh_index(config_file)
    
    -- Activate Zettels Menu
    local menu = textadept.menu.menubar
    menu[#menu + 1] = zettels_menu

    -- And the Zettels Context Menu
    events.connect(events.INITIALIZED, function()
        local context_menu = textadept.menu.context_menu
        context_menu[#context_menu+1] = zettels_context_menu
    end)
end

--- ##########################################################################
--- Public Interface
--- ##########################################################################
local M = {
    enable = enable,
    _VERSION = '0.1.0',
    -- intended to work with zettels v0.3.0
}

return M
